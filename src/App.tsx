import styles from './App.module.sass';
import React, {FunctionComponent, useContext} from 'react';
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import Alerts from "./components/Alerts";
import NavBar from "./components/NavBar";
import Home from "./components/Home";
import {Store} from "./index";
import {observer} from "mobx-react";
import Login from "./components/Login";
import Register from "./components/Register";
import Users from "./components/Users";
import Concerts from "./components/Concerts";

const App: FunctionComponent = () => {
  const {isRedirectHome, toggle, baseURL} = useContext(Store);

  return (
    <BrowserRouter>
      {isRedirectHome && <Redirect to={baseURL}/>}
      <div className={styles.app}>
        <Alerts/>
        <NavBar/>

        <Switch>
          <Route exact path={baseURL}>
            <Home reset={toggle}/>
          </Route>
          <Route path={baseURL + "/login"}>
            <Login/>
          </Route>
          <Route path={baseURL + "/register"}>
            <Register/>
          </Route>
          <Route path={baseURL + "/users"}>
            <Users/>
          </Route>
          <Route path={baseURL + "/concerts"}>
            <Concerts/>
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};


export default observer(App);
