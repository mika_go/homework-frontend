import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import RootStore from "./store";
import 'bootstrap/dist/css/bootstrap.min.css';

export const Store = createContext(new RootStore());

ReactDOM.render(
  <Store.Provider value={new RootStore()}>
    <App/>
  </Store.Provider>, document.getElementById('root'));

serviceWorker.unregister();
