import React, {FunctionComponent, useContext, Fragment, useEffect, useState, FormEvent} from "react";
import {
  Button,
  Container,
  PageItem,
  Pagination,
  Table,
  Modal,
  ModalBody,
  ModalFooter, Form, FormControl
} from "react-bootstrap";
import {Store} from "../../index";
import {observer} from "mobx-react";
import ModalHeader from "react-bootstrap/ModalHeader";
import {action} from "mobx";

const Users: FunctionComponent = () => {
  const {dataStore, alertStore} = useContext(Store);
  const {users, usersPages, fetchUsers, fetchUsersPages, deleteUser} = dataStore;
  const [page, setPage] = useState(0);
  const [change, setChange] = useState(true);
  const [show, setShow] = useState(false);
  const [isEdit, setEdit] = useState(false);

  const [pk, setPk] = useState(-1);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [patronymic, setPatronymic] = useState("");

  const onChangeUsername = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setUsername(event.currentTarget.value);
  };

  const onChangePassword = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  };

  const onChangeName = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setName(event.currentTarget.value);
  };

  const onChangeSurname = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setSurname(event.currentTarget.value);
  };

  const onChangePatronymic = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setPatronymic(event.currentTarget.value);
  };

  const addUser = action(() => {
    if (!(username && password && name && surname && patronymic)) {
      alertStore.alert("warning", "Заполните все поля!");
      return;
    }

    dataStore.postUser(username, password, name, surname, patronymic).then(() => {
      setUsername("");
      setPassword("");
      setName("");
      setSurname("");
      setPatronymic("");
      hideModal();
      setChange(!change);
    });
  });

  const editUser = action((id: number) => {
    if (!(username && password && name && surname && patronymic)) {
      alertStore.alert("warning", "Заполните все поля!");
      return;
    }

    dataStore.putUser(id, username, password, name, surname, patronymic).then(() => {
      setUsername("");
      setPassword("");
      setName("");
      setSurname("");
      setPatronymic("");
      hideModal();
      setEdit(false);
      setChange(!change);
    });
  });

  const showModal = () => setShow(true);
  const hideModal = () => setShow(false);

  useEffect(() => {
    fetchUsers(page);
    fetchUsersPages();
  }, [page, change]);

  const pages = [];
  for (let i = 0; i < usersPages; i++) {
    pages.push(<PageItem key={i} active={i === page} onClick={() => setPage(i)}>{i + 1}</PageItem>)
  }

  return (
    <Container className="mt-3">
      <Button className="mb-3" variant={"success"} onClick={showModal}>Добавить</Button>
      <Table>
        <thead>
        <tr>
          <th>id</th>
          <th>Имя пользователя</th>
          <th>Пароль</th>
          <th>Фамилия</th>
          <th>Имя</th>
          <th>Отчество</th>
          <th></th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {
          users.map((value, index) =>
            <tr key={index}>
              <td>{value.pk}</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.username :
                  <FormControl type={"text"} placeholder="Имя пользователя" value={username} onChange={onChangeUsername}
                               isInvalid={username === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.password :
                  <FormControl type={"text"} placeholder="Пароль" value={password} onChange={onChangePassword}
                               isInvalid={password === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.surname :
                  <FormControl type={"text"} placeholder="Фамилия" value={surname} onChange={onChangeSurname}
                               isInvalid={surname === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.name :
                  <FormControl type={"text"} placeholder="Имя" value={name} onChange={onChangeName}
                               isInvalid={name === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.patronymic :
                  <FormControl type={"text"} placeholder="Отчество" value={patronymic} onChange={onChangePatronymic}
                               isInvalid={patronymic === ""}/>
              }</td>
              <td>
                {
                  isEdit && value.pk === pk ? <Button variant={"success"} onClick={() => editUser(value.pk)}>Сохранить</Button> :
                    <Button variant={"secondary"} onClick={() => {
                      setPk(value.pk);
                      setUsername(value.fields.username);
                      setPassword(value.fields.password);
                      setSurname(value.fields.surname);
                      setName(value.fields.name);
                      setPatronymic(value.fields.patronymic);
                      setEdit(true);
                    }}>Изменить</Button>
                }
              </td>
              <td><Button variant={"danger"} onClick={() => {
                deleteUser(value.pk).then(() => {
                  setChange(!change);
                });
              }}>X</Button></td>
            </tr>
          )
        }
        </tbody>
      </Table>
      <Pagination>
        {
          pages
        }
      </Pagination>

      <Modal show={show} onHide={hideModal}>
        <ModalHeader closeButton>
          <Modal.Title>Добавить пользователя</Modal.Title>
        </ModalHeader>

        <ModalBody>
          <Form>
            <Form.Group controlId="formBasicUsername">
              <Form.Label>Имя пользователя</Form.Label>
              <FormControl type={"text"} placeholder="Имя пользователя" value={username} onChange={onChangeUsername}
                           isInvalid={username === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Пароль</Form.Label>
              <FormControl type="password" placeholder="Пароль" value={password} onChange={onChangePassword}
                           isInvalid={password === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicSurname">
              <Form.Label>Фамилия</Form.Label>
              <FormControl type={"text"} placeholder="Фамилия" value={surname} onChange={onChangeSurname}
                           isInvalid={surname === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicName">
              <Form.Label>Имя</Form.Label>
              <FormControl type={"text"} placeholder="Имя" value={name} onChange={onChangeName}
                           isInvalid={name === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicPatronymic">
              <Form.Label>Отчество</Form.Label>
              <FormControl type={"text"} placeholder="Отчество" value={patronymic} onChange={onChangePatronymic}
                           isInvalid={patronymic === ""}/>
            </Form.Group>
          </Form>
        </ModalBody>

        <ModalFooter>
          <Button variant="secondary" onClick={hideModal}>Закрыть</Button>
          <Button variant={"success"} onClick={addUser}>Добавить</Button>
        </ModalFooter>
      </Modal>
    </Container>
  );
};

export default observer(Users);
