import React, {FormEvent, FunctionComponent, useContext, useState} from "react";
import {Button, Container, Form, FormControl} from "react-bootstrap";
import {Store} from "../../index";
import {action} from "mobx";

const Register: FunctionComponent = () => {
  const {authStore, alertStore} = useContext(Store);
  const [username, setUsername] = useState(authStore.username);
  const [password, setPassword] = useState(authStore.password);
  const [name, setName] = useState(authStore.name);
  const [surname, setSurname] = useState(authStore.surname);
  const [patronymic, setPatronymic] = useState(authStore.patronymic);

  const onChangeUsername = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setUsername(event.currentTarget.value);
  };

  const onChangePassword = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  };

  const onChangeName = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setName(event.currentTarget.value);
  };

  const onChangeSurname = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setSurname(event.currentTarget.value);
  };

  const onChangePatronymic = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setPatronymic(event.currentTarget.value);
  };

  const onClick = action(() => {
    if (!(username && password && name && surname && patronymic)) {
      alertStore.alert("warning", "Заполните все поля!")
      return;
    }
    authStore.username = username;
    authStore.password = password;
    authStore.name = name;
    authStore.surname = surname;
    authStore.patronymic = patronymic;

    authStore.register();
  });

  return (
    <Container className="mt-3">
      <Form>
        <Form.Group controlId="formBasicUsername">
          <Form.Label>Имя пользователя</Form.Label>
          <FormControl type={"text"} placeholder="Имя пользователя" value={username} onChange={onChangeUsername}
                       isInvalid={username === ""}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Пароль</Form.Label>
          <FormControl type="password" placeholder="Пароль" value={password} onChange={onChangePassword}
                       isInvalid={password === ""}/>
        </Form.Group>

        <Form.Group controlId="formBasicSurname">
          <Form.Label>Фамилия</Form.Label>
          <FormControl type={"text"} placeholder="Фамилия" value={surname} onChange={onChangeSurname}
                       isInvalid={surname === ""}/>
        </Form.Group>

        <Form.Group controlId="formBasicName">
          <Form.Label>Имя</Form.Label>
          <FormControl type={"text"} placeholder="Имя" value={name} onChange={onChangeName}
                       isInvalid={name === ""}/>
        </Form.Group>

        <Form.Group controlId="formBasicPatronymic">
          <Form.Label>Отчество</Form.Label>
          <FormControl type={"text"} placeholder="Отчество" value={patronymic} onChange={onChangePatronymic}
                       isInvalid={patronymic === ""}/>
        </Form.Group>

        <Button variant="primary" onClick={onClick}>Войти</Button>
      </Form>
    </Container>
  );
};

export default Register;
