import React, {FunctionComponent, useEffect} from 'react';
import {Container} from "react-bootstrap";

interface Props {
  reset: (value?: boolean) => void;
}

const Home: FunctionComponent<Props> = ({reset}) => {
  useEffect(() => reset(false));

  return (
    <Container className="mt-3">
      <h5>Домашнее задание по РИП</h5>
      <p>
        Чтобы увидеть данные приложения необходимо авторизоваться. Если Вы еще не зарегистрированы, то можете зарегистрироваться.
      </p>
      <p>
        Для того, чтобы увидеть данные перейдите на соответствующие вкладки.
      </p>
    </Container>
  );
};

export default Home;
