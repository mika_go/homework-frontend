import React, {FormEvent, FunctionComponent, useContext, useState} from "react";
import {Button, Container, Form, FormControl} from "react-bootstrap";
import {Store} from "../../index";
import {observer} from "mobx-react";
import {action} from "mobx";

const Login: FunctionComponent = () => {
  const {authStore} = useContext(Store);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const onChangeUsername = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setUsername(event.currentTarget.value);
  };

  const onChangePassword = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setPassword(event.currentTarget.value);
  };

  const onClick = action(() => {
    authStore.username = username;
    authStore.password = password;

    authStore.login();
  });

  return (
    <Container className="mt-3">
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Имя пользователя</Form.Label>
          <FormControl type={"text"} placeholder="Имя пользователя" value={username} onChange={onChangeUsername}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Пароль</Form.Label>
          <FormControl type="password" placeholder="Пароль" value={password} onChange={onChangePassword}/>
        </Form.Group>
        <Button variant="primary" onClick={onClick}>Войти</Button>
      </Form>
    </Container>
  );
};

export default observer(Login);
