import React, {FunctionComponent, useContext, Fragment, useEffect, useState, FormEvent} from "react";
import {
  Button,
  Container,
  PageItem,
  Pagination,
  Table,
  Modal,
  ModalBody,
  ModalFooter, Form, FormControl
} from "react-bootstrap";
import {Store} from "../../index";
import {observer} from "mobx-react";
import ModalHeader from "react-bootstrap/ModalHeader";
import {action} from "mobx";

const Concerts: FunctionComponent = () => {
  const {dataStore, alertStore} = useContext(Store);
  const {concerts, concertsPages, fetchConcerts, fetchConcertsPages, deleteConcert} = dataStore;
  const [page, setPage] = useState(0);
  const [change, setChange] = useState(true);
  const [show, setShow] = useState(false);
  const [isEdit, setEdit] = useState(false);

  const [pk, setPk] = useState(-1);
  const [title, setTitle] = useState("");
  const [date, setDate] = useState("");
  const [cost, setCost] = useState(0);
  const [users, setUsers] = useState("");

  const onChangeTitle = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setTitle(event.currentTarget.value);
  };

  const onChangeDate = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setDate(event.currentTarget.value);
  };

  const onChangeCost = (event: FormEvent<FormControl & HTMLInputElement>) => {
    if (!event.currentTarget.value) setCost(0);
    else setCost(+event.currentTarget.value);
  };

  const onChangeUsers = (event: FormEvent<FormControl & HTMLInputElement>) => {
    setUsers(event.currentTarget.value);
  };

  const addConcert = action(() => {
    if (!(title && date && cost && users)) {
      alertStore.alert("warning", "Заполните все поля!");
      return;
    }

    dataStore.postConcert(title, new Date(date), cost, users.split(", ").map(v => +v)).then(() => {
      setTitle("");
      setDate("");
      setCost(0);
      setUsers("");
      hideModal();
      setChange(!change);
    });
  });

  const editConcert = action((id: number) => {
    if (!(title && date && cost && users)) {
      alertStore.alert("warning", "Заполните все поля!");
      return;
    }

    dataStore.putConcert(id, title, new Date(date), cost, users.split(", ").map(v => +v)).then(() => {
      setTitle("");
      setDate("");
      setCost(0);
      setUsers("");
      hideModal();
      setEdit(false);
      setChange(!change);
    });
  });

  const showModal = () => setShow(true);
  const hideModal = () => setShow(false);

  useEffect(() => {
    fetchConcerts(page);
    fetchConcertsPages();
  }, [page, change]);

  const pages = [];
  for (let i = 0; i < concertsPages; i++) {
    pages.push(<PageItem key={i} active={i === page} onClick={() => setPage(i)}>{i + 1}</PageItem>)
  }

  return (
    <Container className="mt-3">
      <Button className="mb-3" variant={"success"} onClick={showModal}>Добавить</Button>
      <Table>
        <thead>
        <tr>
          <th>id</th>
          <th>Названиие</th>
          <th>Дата</th>
          <th>Цена билета</th>
          <th>Пользователи</th>
          <th></th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        {
          concerts.map((value, index) =>
            <tr key={index}>
              <td>{value.pk}</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.title :
                  <FormControl type={"text"} placeholder="Название" value={title} onChange={onChangeTitle}
                               isInvalid={title === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.date.toString() :
                  <FormControl type={"text"} placeholder="Дата" value={date} onChange={onChangeDate}
                               isInvalid={date === ""}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.cost :
                  <FormControl type={"text"} placeholder="Цена билета" value={`${cost}`} onChange={onChangeCost}
                               isInvalid={cost === 0}/>
              }</td>
              <td>{
                !isEdit || value.pk !== pk ? value.fields.users.sort().join(", ") :
                  <FormControl type={"text"} placeholder="Пользователи" value={users} onChange={onChangeUsers}
                               isInvalid={users === ""}/>
              }</td>
              <td>
                {
                  isEdit && value.pk === pk ? <Button variant={"success"} onClick={() => editConcert(value.pk)}>Сохранить</Button> :
                    <Button variant={"secondary"} onClick={() => {
                      setPk(value.pk);
                      setTitle(value.fields.title);
                      setDate(value.fields.date.toString());
                      setCost(value.fields.cost);
                      setUsers(value.fields.users.join(", "));
                      setEdit(true);
                    }}>Изменить</Button>
                }
              </td>
              <td><Button variant={"danger"} onClick={() => {
                deleteConcert(value.pk);
                setChange(!change);
              }}>X</Button></td>
            </tr>
          )
        }
        </tbody>
      </Table>
      <Pagination>
        {
          pages
        }
      </Pagination>

      <Modal show={show} onHide={hideModal}>
        <ModalHeader closeButton>
          <Modal.Title>Добавить пользователя</Modal.Title>
        </ModalHeader>

        <ModalBody>
          <Form>
            <Form.Group controlId="formBasicTitle">
              <Form.Label>Название</Form.Label>
              <FormControl type={"text"} placeholder="Название" value={title} onChange={onChangeTitle}
                           isInvalid={title === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicDate">
              <Form.Label>Дата</Form.Label>
              <FormControl type="text" placeholder="Дата" value={date} onChange={onChangeDate}
                           isInvalid={date === ""}/>
            </Form.Group>

            <Form.Group controlId="formBasicCost">
              <Form.Label>Цена</Form.Label>
              <FormControl type={"text"} placeholder="Цена" value={`${cost}`} onChange={onChangeCost}
                           isInvalid={cost === 0}/>
            </Form.Group>

            <Form.Group controlId="formBasicUsers">
                <Form.Label>Пользователи</Form.Label>
                <FormControl type={"text"} placeholder="Пользователи" value={users} onChange={onChangeUsers}
                             isInvalid={users === ""}/>
            </Form.Group>
          </Form>
        </ModalBody>

        <ModalFooter>
          <Button variant="secondary" onClick={hideModal}>Закрыть</Button>
          <Button variant={"success"} onClick={addConcert}>Добавить</Button>
        </ModalFooter>
      </Modal>
    </Container>
  );
};

export default observer(Concerts);
