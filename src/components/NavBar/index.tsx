import React, {FunctionComponent, useContext} from 'react';
import {Button, Form, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import {observer} from "mobx-react";
import {Store} from "../../index";

const NavBar: FunctionComponent = () => {
  const {authStore, baseURL} = useContext(Store);

  const {isLogged, logout} = authStore;

  return (
    <Navbar bg="light" expand="lg">
      <Link to={baseURL}><Navbar.Brand>РИП</Navbar.Brand></Link>
      <Navbar.Collapse>
        {
          isLogged &&
          <Nav className="mr-auto">
              <Link to={baseURL + "/users"}><Nav.Link as={"div"}>Пользователи</Nav.Link></Link>
              <Link to={baseURL + "/concerts"}><Nav.Link as={"div"}>Концерты</Nav.Link></Link>
          </Nav>
        }
        {
          !isLogged &&
          <Form className="ml-auto" inline>
              <Link to={baseURL + "/login"}>
                  <Button variant="outline-primary">Войти</Button>
              </Link>
              <Link to={baseURL + "/register"}>
                  <Button className="ml-1" variant="outline-primary">Регистрация</Button>
              </Link>
          </Form>
        }
        {
          isLogged &&
          <Button variant={"outline-primary"} onClick={logout}>Выйти</Button>
        }
      </Navbar.Collapse>
    </Navbar>
  );
};

export default observer(NavBar);
