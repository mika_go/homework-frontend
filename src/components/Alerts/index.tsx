import React, {FunctionComponent, useContext} from 'react';
import styles from "./style.module.sass";
import {observer} from "mobx-react";
import {Alert} from "react-bootstrap";
import {Store} from "../../index";

const Alerts: FunctionComponent = () => {
  const {alertStore} = useContext(Store);
  const {alerts} = alertStore;

  return (
    <div className={styles.container}>
      <div className={styles.alerts}>
        {
          alerts.map(({variant, text}, index) =>
            <Alert key={index} variant={variant}>{text}</Alert>
          )
        }
      </div>
    </div>
  );
};

export default observer(Alerts);
