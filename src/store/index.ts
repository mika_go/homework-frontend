import AlertStore from "./AlertStore";
import AuthStore from "./AuthStore";
import {action, observable} from "mobx";
import DataStore from "./DataStore";

class RootStore {
  @observable isRedirectHome = false;
  baseURL = "";
  alertStore: AlertStore;
  authStore: AuthStore;
  dataStore: DataStore;

  constructor() {
    this.baseURL = document.location.pathname;
    this.alertStore = new AlertStore(this);
    this.authStore = new AuthStore(this);
    this.dataStore = new DataStore(this);
    this.toggle(true);
  }

  @action
  toggle = (value?: boolean) => {
    if (value !== undefined) {
      this.isRedirectHome = value;
    } else {
      this.isRedirectHome = !this.isRedirectHome;
    }
  }
}

export default RootStore;
