import {action, observable} from "mobx";
import axios from "axios";
import {baseURL} from "../types";
import RootStore from "../index";
import AlertStore from "../AlertStore";

class AuthStore {
  @observable isLogged: boolean = false;
  @observable username: string = "";
  @observable password: string = "";
  @observable name: string = "";
  @observable surname: string = "";
  @observable patronymic: string = "";

  constructor(public root: RootStore) {
    this.checkAuth();
  }

  get alertStore(): AlertStore {
    return this.root.alertStore;
  }

  @action
  checkAuth = () => {
    axios.get(baseURL + "/api/auth").then(() => {
      this.isLogged = true;
    }).catch(() => {
      this.isLogged = false;
    })
  };

  @action
  login = (username = this.username, password = this.password) => {
    axios.post(baseURL + "/api/login", {
      username: username,
      password: password
    }).then(() => {
      this.isLogged = true;
      this.alertStore.alert("success", "Авторизация успешно выполнена!")
    }).catch(() => {
      this.alertStore.alert("danger", "Нет пользователя с такими логином и паролем!")
    }).finally(() => {
      this.root.toggle(true);
    });
  };

  @action
  register = (username = this.username, password = this.password, name = this.name, surname = this.surname, patronymic = this.patronymic) => {
    axios.post(baseURL + "/api/register", {
      username: username,
      password: password,
      name: name,
      surname: surname,
      patronymic: patronymic
    }).then(() => {
      this.isLogged = true;
      this.alertStore.alert("success", "Регистрация успешно выполнена!");
    }).catch(() => {
      this.alertStore.alert("danger", "Ошибка регистрации!");
    }).finally(() => {
      this.root.toggle(true);
    });
  };

  @action
  logout = () => {
    axios.get(baseURL + "/api/logout").then(() => {
      this.isLogged = false;
      this.root.toggle(true);
    });
  };
}

export default AuthStore;
