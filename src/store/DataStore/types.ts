export interface User {
  pk: number;
  fields: {
    username: string;
    password: string;
    name: string;
    surname: string;
    patronymic: string;
  }
}

export interface Concert {
  pk: number;
  fields: {
    title: string;
    date: Date;
    cost: number;
    users: number[];
  }
}
