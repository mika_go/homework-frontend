import {Concert, User} from "./types";
import RootStore from "../index";
import {action, IObservableArray, observable} from "mobx";
import axios from "axios";
import {baseURL} from "../types";
import AlertStore from "../AlertStore";

class DataStore {
  @observable users: User[] = [];
  @observable concerts: Concert[] = [];
  @observable usersPages: number = 0;
  @observable concertsPages: number = 0;
  @observable currentUser?: User;
  @observable currentConcert?: Concert;

  constructor(public root: RootStore) {
  }

  get alertStore(): AlertStore {
    return this.root.alertStore;
  }

  @action
  chooseUser = (user: User) => {
    this.currentUser = user;
  };

  @action
  fetchUsers = (page: number = 0) => {
    axios.get(baseURL + `/api/users?page=${page}`).then((res) => {
      (this.users as IObservableArray).replace(res.data);
    })
  };

  @action
  postUser = async (username: string, password: string, name: string, surname: string, patronymic: string) => {
    try {
      await axios.post(baseURL + "/api/users", {
        username: username,
        password: password,
        name: name,
        surname: surname,
        patronymic: patronymic
      });
      this.alertStore.alert("success", "Добавление пользователя успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка добавления пользователя!");
    }
  };

  @action
  putUser = async (id: number, username: string, password: string, name: string, surname: string, patronymic: string) => {
    try {
      await axios.put(baseURL + `/api/users?id=${id}`, {
        username: username,
        password: password,
        name: name,
        surname: surname,
        patronymic: patronymic
      });
      this.alertStore.alert("success", "Изменение пользователя успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка изменения пользователя!");
    }
  };

  @action
  deleteUser = async (id: number) => {
    try {
      await axios.delete(baseURL + `/api/users?id=${id}`);
      this.alertStore.alert("success", "Удаление пользователя успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка удаления пользователя!");
    }
  };

  @action
  fetchUsersPages = () => {
    axios.get(baseURL + "/api/pages/users").then((res) => {
      this.usersPages = res.data.pages;
    })
  };

  @action
  chooseConcert = (concert: Concert) => {
    this.currentConcert = concert;
  };

  @action
  fetchConcerts = (page: number = 0) => {
    axios.get(baseURL + `/api/concerts?page=${page}`).then((res) => {
      const data: Concert[] = res.data.map((value: any) => {
        value.fields.date = new Date(value.fields.date);
        return value;
      });
      (this.concerts as IObservableArray).replace(data);
    })
  };

  @action
  postConcert = async (title: string, date: Date, cost: number, users: number[]) => {
    try {
      await axios.post(baseURL + "/api/concerts", {
        title: title,
        date: date,
        cost: cost,
        users: users
      });
      this.alertStore.alert("success", "Добавление концерта успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка добавления концерта!");
    }
  };

  @action
  putConcert = async (id: number, title: string, date: Date, cost: number, users: number[]) => {
    try {
      await axios.put(baseURL + `/api/concerts?id=${id}`, {
        title: title,
        date: date,
        cost: cost,
        users: users
      });
      this.alertStore.alert("success", "Изменение концерта успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка изменения концерта!");
    }
  };

  @action
  deleteConcert = async (id: number) => {
    try {
      await axios.delete(baseURL + `/api/concerts?id=${id}`);
      this.alertStore.alert("success", "Удаление концерта успешно!");
    } catch {
      this.alertStore.alert("danger", "Ошибка удаления концерта!");
    }
  };

  @action
  fetchConcertsPages = () => {
    axios.get(baseURL + "/api/pages/concerts").then((res) => {
      this.concertsPages = res.data.pages;
    })
  };
}

export default DataStore;
