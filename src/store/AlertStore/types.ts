export type IAlertVariant =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'dark'
  | 'light';

export interface IAlert {
  id: number;
  variant: IAlertVariant;
  text: string;
}
