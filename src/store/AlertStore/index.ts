import {action, IObservableArray, observable} from "mobx";
import {IAlert, IAlertVariant} from "./types";
import RootStore from "../index";

class AlertStore {
  @observable alerts: IAlert[] = [];
  id = 0;
  timeout = 3000;

  constructor(public root: RootStore) {
  }

  @action
  alert = (variant: IAlertVariant, text: string) => {
    const alert = {id: this.id, variant: variant, text: text};
    this.alerts.push(alert);
    setTimeout((id: number) => {
      const alert = this.alerts.find(v => v.id === id);
      (this.alerts as IObservableArray).remove(alert);
    }, this.timeout, this.id++)
  };
}

export default AlertStore;
